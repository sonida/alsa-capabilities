NAME			= alsa-capabilities
VERSION			:= $(shell grep APP_VERSION= ${NAME} | awk -F\" '{print $$2}')
#VERSION		:= 1.0.2
GIT_VERSION		:= v${VERSION}

TARBALL_DIR		:= tmp
TARBALL_NAME		= ${NAME}-v${VERSION}
TARBALL_TARGET		= ${TARBALL_DIR}/${TARBALL_NAME}.tar.gz
TARBALL_SIG		= ${TARBALL_DIR}/${TARBALL_NAME}.asc

PREFIX			:= build/usr
DOC_DIR			:= ${PREFIX}/share/doc/${NAME}
MAN_DIR			:= ${PREFIX}/share/man/man1
BIN_DIR			:= ${PREFIX}/bin
CHANGELOG_DIR		:= ${PREFIX}/share/${NAME}

BIN_SOURCE 		= ${NAME}
BIN_TARGET 		:= ${BIN_DIR}/${BIN_SOURCE}

README_SOURCE		= README.md
README_TARGET		:= ${DOC_DIR}/README.gz

MAN_SOURCE		= ${NAME}.1.md
MAN_TARGET		:= ${MAN_DIR}/${NAME}.1.gz

GIT_TAG			:= .git/refs/tags/${VERSION}
DIST_ARCH		:= dists/arch
DIST_ARCH_PKGBUILD 	:= ${DIST_ARCH}/PKGBUILD
DIST_ARCH_PKGBUILD_IN 	:= ${DIST_ARCH}/PKGBUILD.in
DIST_ARCH_PKGBUILD_VERS	:= ${DIST_ARCH}/PKGBUILD.${GIT_VERSION}
DIST_ARCH_SRCINFO 	:= ${DIST_ARCH}/.SRCINFO
DIST_ARCH_TARBALLS	:= $(wildcard ${DIST_ARCH}/*.tar.gz)

CHANGELOG_TARGET 	= ${CHANGELOG_DIR}/CHANGELOG.gz

.PHONY: git-tag build sign clean check tag release install uninstall all arch-release clean-arch-dist CHANGELOG

tarball: ${TARBALL_SIG}

arch-release: ${GIT_TAG} ${DIST_ARCH_SRCINFO}

clean-arch-dist:
	@echo "*** removing previously generated Arch PKGBUILD and .SRCINFO files ... "
	rm -vf ${DIST_ARCH_PKGBUILD} ${DIST_ARCH_SRCINFO} ${DIST_ARCH_PKGBUILD_VERS} ${DIST_ARCH_TARBALLS}

${DIST_ARCH_PKGBUILD_VERS}: ${DIST_ARCH_PKGBUILD_IN}  clean-arch-dist
	@echo "*** copying PKGBUILD.in template to versioned PKGBUILD.${GIT_VERSION}"
	cp -av $< $@ 
	sed -i "s/pkgver=v0.0.0/pkgver=${GIT_VERSION}/" $@

${DIST_ARCH_PKGBUILD}: ${DIST_ARCH_PKGBUILD_VERS}
	@echo "*** copying versioned PKGBUILD.${GIT_VERSION} to PKGBUILD"
	cp -av $< $@
	@echo "*** replacing placeholders in PKGBUILD"
	sed -i "s/sha256sums=('justaplaceholder')/$$(cd ${DIST_ARCH} && makepkg --geninteg)/" $@

${DIST_ARCH_SRCINFO}: ${DIST_ARCH_PKGBUILD}
	cd ${DIST_ARCH}; makepkg --printsrcinfo > .SRCINFO

CHANGELOG: 
	git log --decorate > $@

${GIT_TAG}: CHANGELOG
	git commit -m 'preparing new release' -a
	git tag -af ${GIT_VERSION} -m "new release ${GIT_VERSION}"
	git push origin ${GIT_VERSION}

${TARBALL_SIG}: ${TARBALL_TARGET}
	gpg --batch --yes --sign --detach-sign --armor $<

${TARBALL_TARGET}: 
	mkdir -p ${TARBALL_DIR}
	git archive --output=$@ --prefix=${TARBALL_NAME}/ ${GIT_VERSION}

clean-tarball:
	rm -f ${TARBALL_TARGET} ${TARBALL_SIG}


${CHANGELOG_DIR} ${MAN_DIR} ${BIN_DIR} ${DOC_DIR}:
	mkdir -p $@

${MAN_TARGET}: ${MAN_SOURCE} ${MAN_DIR}
	pandoc $< --standalone --to man | gzip - > $@

${CHANGELOG_TARGET}: ${CHANGELOG_DIR} 
	git log --decorate | gzip - > $@

${README_TARGET}: ${README_SOURCE} ${DOC_DIR}
	pandoc $< --standalone --to plain | gzip - > $@

${BIN_TARGET}: ${BIN_SOURCE} ${BIN_DIR}
	install --verbose --mode=0755 $< $@

install: ${MAN_TARGET} ${CHANGELOG_TARGET} ${README_TARGET} ${BIN_TARGET}

uninstall:
	rm -rf ${PREFIX}

check:
	shellcheck ${BIN_SOURCE}

localtag:
	git tag --force v${VERSION}

remotetag: localtag
	git push --tags

#release: ${PKG} ${SIG} localtag


